import React, { FunctionComponent } from 'react';
import './Output.css';

type OutputProps = {
  content: string;
};

const Output: FunctionComponent<OutputProps> = ({ content }: OutputProps) => {
  const parsed = content.replace(/(.com|.org)/, '$1 ');
  const matches =
    parsed.match(/[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]+/g) || [];
  const emails = matches.map((email, key) => <li key={key}>{email}</li>);

  return (
    <section className="Output">
      <h1 className="Output__header">Emails Found:</h1>
      <ul className="Output__emails">{emails}</ul>
    </section>
  );
};

export default Output;
