import React, { ChangeEvent } from 'react';
import './Input.css';

type InputProps = {
  content: string;
  onContentChange: Function;
};

class Input extends React.Component<InputProps> {
  constructor(props: InputProps) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(e: ChangeEvent<HTMLTextAreaElement>): void {
    this.props.onContentChange(e.target.value);
  }

  render(): JSX.Element {
    const content = this.props.content;

    return <textarea value={content} onChange={this.handleChange}></textarea>;
  }
}

export default Input;
