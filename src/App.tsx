import React from 'react';
import './App.css';
import Header from './Header/Header';
import Input from './Input/Input';
import Output from './Output/Output';

class App extends React.Component<{}, { content: string }> {
  constructor(props: any) {
    super(props);
    this.state = { content: "" };
    this.handleContentChange = this.handleContentChange.bind(this);
  }

  handleContentChange(content: string) {
    this.setState({ content });
  }

  render(): JSX.Element {
    const content = this.state.content;
    const onContentChange = this.handleContentChange;

    return (
      <div className="App">
        <Header title="Collect Emails"></Header>
        <main className="App__main">
          <Input content={content} onContentChange={onContentChange}></Input>
          <Output content={content}></Output>
        </main>
      </div>
    );
  }
}

export default App;
