import React, { FunctionComponent } from 'react';
import './Header.css';

type HeaderProps = {
  title: string;
};

const Header: FunctionComponent<HeaderProps> = ({ title }: HeaderProps) => (
  <header className="header">
    <h1 className="header__brand">{title}</h1>
  </header>
);

export default Header;
